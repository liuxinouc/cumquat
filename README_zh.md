# 金桔图形界面系统

#### 介绍
金桔龟是一个仅有2200行代码的极小GUI系统；创作金桔龟是为了给前端开发开辟一条不依赖美工的技术路线。
金桔龟（CumquatGUI）的特点如下：
1、它是纯 canvas GUI；
2、它只支持鼠标操作；
3、它支持很多种类的控件，包括：Button、List、ScrollBar、ProgressBar、CheckBox、Spin、Combo、ImageLabel 和 TextLabel；
4、它支持模态对话框；
5、它使用布局器来规划控件位置。

#### 软件使用说明
开箱即用，在网页中引入 cumquat.js 即可使用

<script src="js/cumquat.js"></script>

使用方法请参见 index_zh.html 提供的范例
