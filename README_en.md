# Kumquat Graphical Interface System

#### Introduction
CumquatGUI is a very small GUI system with only 2200 lines of code; CumquatGUI was created to open up a technical route that does not rely on art for front-end development.
The characteristics of CumquatGUI are as follows:
1. It is a pure canvas GUI;
2. It only supports mouse operation;
3. It supports many types of controls, including: Button, List, ScrollBar, ProgressBar, CheckBox, Spin, Combo, ImageLabel, and TextLabel;
4. It supports modal dialogs;
5. It uses the layout device to plan the position of the control.

#### Software instructions
Out of the box, you can use cumquat.js in the web page as follows:

<script src="js/cumquat.js"></script>

Please refer to the example provided by index_zh.html for how to use it
